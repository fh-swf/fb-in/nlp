![CC BY NC SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

# Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)
This repository contains example code and exercises for the lecture *Natural Language Processing* at [Fachhochschule Südwestfalen](https://www.fh-swf.de). 
All materials can be used under the terms of the [by-nc-sa licence](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

© [Christian Gawron](mailto:gawron.christian@fh-swf.de) 