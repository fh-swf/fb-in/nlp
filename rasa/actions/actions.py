# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
import python_weather

class ActionWeather(Action):

    def name(self) -> Text:
        return "action_weather"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict]:

        
        location = location = next((e['value'] for e in tracker.latest_message['entities'] if e['entity'] == 'LOC'), None)
        async with python_weather.Client(unit=python_weather.METRIC) as client:
            # fetch a weather forecast from a city
            weather = await client.get(location)
            
            # returns the current day's forecast temperature (int)
            #print(weather.current.temperature)
            
            # get the weather forecast for a few days
            #for forecast in weather.forecasts:
            #  print(forecast)
    
    
            
    
            if weather is None:
                dispatcher.utter_message(text="I could not find the weather for the location: {}".format(location))
                return []
            
            
    
            weather_message = f"The current weather in {location} is {weather.forecasts} with a temperature of {weather.current.temperature}°C."
            dispatcher.utter_message(text=weather_message)

        return []
