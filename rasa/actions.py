import logging
import re

from typing import Dict, Text, Any, List, Union, Optional

from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction

logger = logging.getLogger(__name__)

class AppointmentForm(FormAction):
    """Example of a custom form action"""

    def name(self) -> Text:
        """Unique identifier of the form"""

        return "appointment_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        if tracker.get_slot('contact') == 'email':
            return ["name", "contact", "email"]
        elif tracker.get_slot('contact') == 'phone':
            return ["name", "contact", "phone"]
        else:
            return ["name", "contact"]

    def slot_mappings(self) -> Dict[Text, Union[Dict, List[Dict]]]:
        """A dictionary to map required slots to
            - an extracted entity
            - intent: value pairs
            - a whole message
            or a list of them, where a first match will be picked"""

        return {
            "name": [self.from_entity(entity="PER")],
            "contact": [self.from_entity(entity="contact"), self.from_text()],
            "email": [self.from_entity(entity="email"), self.from_text()],
            "phone": [self.from_entity(entity="phone"), self.from_text()]
        }


    def validate_email(
        self,
        value: Text,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> Dict[Text, Any]:
        """Validate cuisine value."""

        if re.match('([^ \t\n\r\f\v@]+)@([^ \t\n\r\f\v@]+)', value) is not None:
            # validation succeeded, set the value of the "cuisine" slot to value
            return {"email": value}
        else:
            dispatcher.utter_message(template="utter_invalid_email")
            # validation failed, set this slot to None, meaning the
            # user will be asked for the slot again
            return {"email": None}

    
    def submit(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""

        contact = tracker.get_slot('contact')
        logger.debug(f'contact is {contact}')
        dispatcher.utter_message(template="utter_submit")
        return []
    
class ActionConfirmAppointment(Action):
    def name(self):
        # type: () -> Text
        return "action_confirm_appointment"

    def run(self, dispatcher, tracker, domain):
        # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict[Text, Any]]
      
        contact = tracker.get_slot('contact')
        logger.debug(f'contact is {contact}')
        if contact == 'email':
            dispatcher.utter_message(template="utter_confirm_email")
        else:
            dispatcher.utter_message(template="utter_confirm_phone")
        return []
    
class ActionSetName(Action):
    def name(self):
        # type: () -> Text
        return "action_set_name"

    def run(self, dispatcher, tracker, domain):
        # type: (CollectingDispatcher, Tracker, Dict[Text, Any]) -> List[Dict[Text, Any]]
      
        per = tracker.get_slot('PER')
        return [SlotSet("name", per)]