# Modul Natural Language Processing

Dieses Repository enthält Material, Code-Beispiele und Übungsaufgaben zum Modul [Natural Language Processing](https://elearning.fh-swf.de/course/view.php?id=7512) an der 
[Fachhochschule Südwestfalen](https://www.fh-swf.de).

